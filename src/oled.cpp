#if ARDUINO >= 100
    #include "Arduino.h"
#else
    #include "WProgram.h"
#endif

#include <SPI.h>
#include <Wire.h>

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

void showStat(const char *stat, const char *value) {
  display.clearDisplay();

  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.setTextSize(2);

  display.println(stat);

  int sz = 4;
  int value_len = strlen(value);
  if (value_len > 5) {
    sz = 2;
  }
  display.setTextSize(sz);
  display.setTextColor(WHITE);
  display.println(value);

  display.display();
}

void testdrawchar(void) {
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);

  for (uint8_t i=0; i < 168; i++) {
    if (i == '\n') continue;
    display.write(i);
    if ((i > 0) && (i % 21 == 0))
      display.println();
  }
  display.display();
  delay(1);
}


void setup()   {
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();

  testdrawchar();
  delay(5000);

  display.clearDisplay();
}


void loop() {
  for(;;) {
    showStat("Teplota:\n", "451 F");
    delay(5000);
    unsigned long time = millis();
    unsigned long sec = time / 1000;
    int min = sec / 60;
    int hours = min / 60;
    sec = sec % 60;
    min = min % 60;
    hours = hours % 24;
    char buffer[10];
    sprintf(buffer, "\n  %d:%02d:%02d", hours, min, sec);
    showStat("Cas:", buffer);
    delay(5000);
  }
}

#=============================================================================#
# Author: QueezyTheGreat                                                      #
# Date:   26.04.2011                                                          #
#                                                                             #
# Description: Arduino CMake example                                          #
#                                                                             #
#=============================================================================#
set(CMAKE_TOOLCHAIN_FILE cmake/ArduinoToolchain.cmake) # Arduino Toolchain

cmake_minimum_required(VERSION 2.8)
#====================================================================#
#  Setup Project                                                     #
#====================================================================#
project(ArduinoTachometr C CXX)

set(ARDUINO_DEFAULT_BOARD nano328) # Default Board ID, when not specified
set(ARDUINO_DEFAULT_PORT /dev/ttyUSB0) # Default Port, when not specified

# set(${PROJECT_NAME}_PORT /dev/ttyUSB0)
# set(${PROJECT_NAME}_BOARD nano328)
# set(${PROJECT_NAME}_AFLAGS nano328)

# print_board_list()

# print_programmer_list()

add_subdirectory(src)   #add the example directory into build
